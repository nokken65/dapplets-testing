[![pipeline status](https://gitlab.com/nokken65/dapplets-testing/badges/main/pipeline.svg)](https://gitlab.com/nokken65/dapplets-testing/-/commits/main)
[![Latest Release](https://gitlab.com/nokken65/dapplets-testing/-/badges/release.svg)](https://gitlab.com/nokken65/dapplets-testing/-/releases)

# dapplets-testing

Demo - https://dapplets-testing.herokuapp.com/
