import 'reseter.css';
import './styles/index.scss';

import { Routing } from '@/pages';

import { withProviders } from './providers';
import { store } from './state/store';

const PureApp = () => {
  return <Routing />;
};

// eslint-disable-next-line import/no-default-export, @typescript-eslint/no-explicit-any
export default withProviders({ store })(PureApp) as React.ComponentType<any>;
