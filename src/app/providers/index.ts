import { compose, Store } from '@reduxjs/toolkit';

import { withContext } from './withContext';
import { withRouter } from './withRouter';
import { withStore } from './withStore';

type ProvidersProps = {
  store: Store;
};

export const withProviders = ({ store }: ProvidersProps) =>
  compose(withStore(store), withRouter, withContext);
