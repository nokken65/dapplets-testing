import { ReactNode } from 'react';

import { menuModel } from '@/entities/Menu';
import { settingsModel } from '@/entities/Settings';

const providers = [settingsModel.SettingsProvider, menuModel.MenuProvider];

interface Props {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  components: Array<React.JSXElementConstructor<React.PropsWithChildren<any>>>;
  children: React.ReactNode;
}

const Compose = ({ components = [], children }: Props) => {
  return (
    <>
      {components.reduceRight((acc, Comp) => {
        return <Comp>{acc}</Comp>;
      }, children)}
    </>
  );
};

export const withContext = (component: () => ReactNode) => () =>
  <Compose components={providers}>{component()}</Compose>;
