/* eslint-disable import/no-cycle */
import { ConfigureStoreOptions } from '@reduxjs/toolkit';

import { dappletModel } from '@/entities/Dapplet';
import { errorsModel } from '@/entities/Error';
import { listsModel } from '@/entities/List';
import { tagsModel } from '@/entities/Tag';

export const config: ConfigureStoreOptions = {
  reducer: {
    [dappletModel.api.reducerPath]: dappletModel.api.reducer,
    [tagsModel.api.reducerPath]: tagsModel.api.reducer,
    [listsModel.api.reducerPath]: listsModel.api.reducer,
    errors: errorsModel.reducer,
  },
  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(
      dappletModel.api.middleware,
      tagsModel.api.middleware,
      listsModel.api.middleware,
    ),
};
