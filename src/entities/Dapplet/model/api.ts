import { createApi, retry } from '@reduxjs/toolkit/query/react';

import type { ValidResponse } from '@/shared/api';
import { dappletsBaseQuery } from '@/shared/api';
import { delay } from '@/shared/utils';

import type { Dapplet, DappletsRequestParams } from './models';

export const api = createApi({
  reducerPath: 'api/dapplets/dapplets',
  baseQuery: retry(dappletsBaseQuery, {
    maxRetries: 3,
    backoff: () => delay(500),
  }),
  tagTypes: ['Dapplets'],
  endpoints: (builder) => ({
    getDapplets: builder.query<ValidResponse<Dapplet[]>, DappletsRequestParams>(
      {
        query: (params) => ({
          url: 'dapplets',
          method: 'get',
          params,
        }),
        providesTags: ['Dapplets'],
      },
    ),
    getImage: builder.query<ValidResponse<string>, string>({
      query: (name) => ({
        url: `files/${name}`,
        method: 'get',
        timeout: 5000,
        responseType: 'blob',
      }),
      transformResponse: (res: ValidResponse<Blob>) => ({
        ...res,
        data: URL.createObjectURL(res.data),
      }),
      providesTags: ['Dapplets'],
    }),
  }),
});
