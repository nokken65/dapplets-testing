export type Dapplet = {
  [key: string]: string | number | string[];
  id: string;
  icon: string;
  title: string;
  author: string;
  rating: number;
  address: string;
  released: string;
  downloads: number;
  description: string;
  text_1: string;
  text_2: string;
  text_3: string;
  text_4: string;
  text_5: string;
  text_6: string;
  text_7: string;
  text_8: string;
  text_9: string;
  tags: string[];
};

export type DappletField = {
  title: string;
  content: string;
};

export type DappletsRequestParams = {
  start?: number;
  limit?: number;
  filter?: Filter[];
  sort?: Sort[];
};

export type Filter = {
  property: string;
  value: string;
};

export type Sort = {
  property: string;
  direction: 'ASC' | 'DESC';
};
