import { nanoid } from '@reduxjs/toolkit';

import { DappletField } from '../../model/models';
import styles from './index.module.scss';

type ExpandedProps = {
  fields: DappletField[];
};

export const DappletDetails = ({ fields }: ExpandedProps) => {
  return (
    <div className={styles.details}>
      {fields.map((field) =>
        field.content ? (
          <div key={nanoid()}>
            <h4>{field.title}</h4>
            <p>{field.content}</p>
          </div>
        ) : null,
      )}
    </div>
  );
};
