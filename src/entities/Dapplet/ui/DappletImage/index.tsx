import NotFound from '@icons/not-found.svg';

import { Image, Loader } from '@/shared/components';

import { api } from '../../model';
import styles from './index.module.scss';

type DappletImageProps = {
  name: string;
  alt?: string;
};

export const DappletImage = ({ name, alt }: DappletImageProps) => {
  const { data, isLoading, isError } = api.useGetImageQuery(name);
  const src = data?.data ?? '';

  if (isLoading) {
    return <Loader.Shine className={styles.box} />;
  }
  if (isError) {
    return <NotFound height={50} width={50} />;
  }

  return <Image alt={alt} className={styles.box} src={src} />;
};
