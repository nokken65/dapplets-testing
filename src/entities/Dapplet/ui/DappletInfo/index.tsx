import { shortDappletAddress } from '@/shared/utils';

import styles from './index.module.scss';

type DappletInfoProps = {
  title: string;
  address: string;
  description: string;
  author: string;
};

export const DappletInfo = ({
  title,
  address,
  description,
  author,
}: DappletInfoProps) => {
  return (
    <>
      <div>
        <h2 className={styles.title}>{title}</h2>
        <p className={styles.address}>{shortDappletAddress(address)}</p>
      </div>
      <p className={styles.description}>{description}</p>
      <p className={styles.author}>{author}</p>
    </>
  );
};
