import {
  createEntityAdapter,
  createSlice,
  isAnyOf,
  nanoid,
} from '@reduxjs/toolkit';

import { dappletModel } from '@/entities/Dapplet';
import { InvalidResponse } from '@/shared/api';

const errorsAdapter = createEntityAdapter<InvalidResponse>({
  selectId: () => nanoid(),
});

const initialState = errorsAdapter.getInitialState();

const errorsSlice = createSlice({
  name: 'errors',
  initialState,
  reducers: {},
  extraReducers: (builder) =>
    builder.addMatcher(
      isAnyOf(
        dappletModel.api.endpoints.getDapplets.matchRejected,
        dappletModel.api.endpoints.getImage.matchRejected,
      ),
      (state, { payload }) =>
        errorsAdapter.addOne(state, payload as InvalidResponse),
    ),
});

export const { reducer } = errorsSlice;
export { errorsAdapter };
