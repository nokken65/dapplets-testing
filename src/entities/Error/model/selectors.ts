import type { RootState } from '@/app/state/store';

import { errorsAdapter } from './errorsSlice';

export const { selectAll: selectAllErrors } =
  errorsAdapter.getSelectors<RootState>((state) => state.errors);
