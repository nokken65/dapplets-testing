import { createApi } from '@reduxjs/toolkit/query/react';

import type { List } from '@/entities/List';
import { mocksBaseQuery, ValidResponse } from '@/shared/api';

export const api = createApi({
  reducerPath: 'api/mocks/lists',
  baseQuery: mocksBaseQuery,
  tagTypes: ['Lists'],
  endpoints: (builder) => ({
    getLists: builder.query<ValidResponse<List[]>, void>({
      query: () => ({
        url: 'lists.json',
        method: 'get',
      }),
      providesTags: ['Lists'],
    }),
  }),
});
