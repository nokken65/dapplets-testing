export type Author = {
  name: string;
  url: string;
};

export type List = {
  id: string;
  title: string;
  author: Author;
};
