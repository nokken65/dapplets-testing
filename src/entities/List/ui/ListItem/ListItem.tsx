import styles from './ListItem.module.scss';

type ListItemProps = {
  title: string;
  author: {
    name: string;
    url: string;
  };
};

export const ListItem = ({ title, author }: ListItemProps) => {
  return (
    <p className={styles.item}>
      {title} (<a href={author.url}>{author.name}</a>)
    </p>
  );
};
