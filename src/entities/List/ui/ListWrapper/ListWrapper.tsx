import { List, Lists } from '@/entities/List';

import styles from './ListWrapper.module.scss';

type ListWrapperProps = {
  lists: List[];
};

export const ListWrapper = ({ lists }: ListWrapperProps) => {
  return (
    <ul className={styles.list}>
      {lists.map((list) => (
        <li key={list.id}>
          <Lists.Item author={list.author} title={list.title} />
        </li>
      ))}
    </ul>
  );
};
