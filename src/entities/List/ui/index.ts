import { ListItem } from './ListItem';
import { ListWrapper } from './ListWrapper';

export const Lists = {
  List: ListWrapper,
  Item: ListItem,
};
