import {
  createContext,
  PropsWithChildren,
  useCallback,
  useContext,
  useMemo,
} from 'react';

import { useLocalStorage } from '@/shared/hooks';

type State = {
  isExpanded: boolean;
  toggle: () => void;
};

type MenuProviderProps = PropsWithChildren<{}>;

const MenuContext = createContext<State | null>(null);

const MenuProvider = ({ children }: MenuProviderProps) => {
  const [state, setState] = useLocalStorage('left_menu_expanded', true);

  const toggle = useCallback(() => setState(!state), [state, setState]);

  const value = useMemo(() => ({ isExpanded: state, toggle }), [state, toggle]);

  return <MenuContext.Provider value={value}>{children}</MenuContext.Provider>;
};

const useMenu = () => {
  const context = useContext(MenuContext);
  if (context === null) {
    throw new Error('useMenu must be used within a MenuProvider');
  }

  return context;
};

export { MenuProvider, useMenu };
