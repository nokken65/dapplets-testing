import Arrow from '@icons/arrow.svg';
import Burger from '@icons/burger.svg';

import { Button } from '@/shared/components';

import { useMenu } from '../../model';
import styles from './index.module.scss';

export const SwitchButton = () => {
  const { isExpanded, toggle } = useMenu();

  return (
    <Button
      className={styles.button}
      icon={isExpanded ? <Arrow /> : <Burger />}
      onClick={toggle}
    />
  );
};
