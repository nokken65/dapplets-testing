import {
  createContext,
  PropsWithChildren,
  useCallback,
  useContext,
  useMemo,
} from 'react';

import { useLocalStorage } from '@/shared/hooks';

type State = {
  isExpanded: boolean;
  toggle: () => void;
};

type SettingsProviderProps = PropsWithChildren<{}>;

const SettingsContext = createContext<State | null>(null);

const SettingsProvider = ({ children }: SettingsProviderProps) => {
  const [state, setState] = useLocalStorage('settings_menu_expanded', true);

  const toggle = useCallback(() => setState(!state), [state, setState]);

  const value = useMemo(() => ({ isExpanded: state, toggle }), [state, toggle]);

  return (
    <SettingsContext.Provider value={value}>
      {children}
    </SettingsContext.Provider>
  );
};

const useSettings = () => {
  const context = useContext(SettingsContext);
  if (context === null) {
    throw new Error('useSettings must be used within a SettingsProvider');
  }

  return context;
};

export { SettingsProvider, useSettings };
