import Arrow from '@icons/arrow.svg';

import { Button } from '@/shared/components';

import { useSettings } from '../../model';
import styles from './index.module.scss';

export const CloseButton = () => {
  const { toggle } = useSettings();

  return <Button className={styles.button} icon={<Arrow />} onClick={toggle} />;
};
