import Settings from '@icons/settings.svg';

import { Button } from '@/shared/components';

import { useSettings } from '../../model';
import styles from './index.module.scss';

export const SwitchButton = () => {
  const { toggle } = useSettings();

  return (
    <Button
      className={styles.button}
      icon={<Settings />}
      label='Settings'
      onClick={toggle}
    />
  );
};
