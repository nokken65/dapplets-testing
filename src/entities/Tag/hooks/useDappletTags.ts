import { Tag, tagsModel } from '@/entities/Tag';

const useDappletTags = (tagIds: string[]): Tag[] => {
  const { data } = tagsModel.api.useGetTagsQuery();
  const tags = data?.data ?? [];

  if (tags.length !== 0) {
    return tags.filter((tag) => tagIds.includes(tag.id));
  }

  return [];
};

export { useDappletTags };
