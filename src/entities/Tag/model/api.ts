import { createApi } from '@reduxjs/toolkit/query/react';

import { dappletsBaseQuery, ValidResponse } from '@/shared/api';

import { Tag } from './models';

export const api = createApi({
  reducerPath: 'api/dapplets/tags',
  baseQuery: dappletsBaseQuery,
  tagTypes: ['Tags'],
  endpoints: (builder) => ({
    getTags: builder.query<ValidResponse<Tag[]>, void>({
      query: () => ({
        url: 'tags',
        method: 'get',
      }),
      providesTags: ['Tags'],
    }),
  }),
});
