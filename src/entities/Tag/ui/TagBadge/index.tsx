import Remove from '@icons/remove.svg';
import clsx from 'clsx';

import { Button } from '@/shared/components';

import styles from './index.module.scss';

type TagBadgeProps = {
  label: string;
  color?: 'violet' | 'teal';
  onRemove?: () => void;
};

export const TagBadge = ({
  label,
  color = 'violet',
  onRemove,
}: TagBadgeProps) => {
  return (
    <span className={clsx(styles.tag, styles[color])}>
      <p>{label}</p>
      {onRemove && (
        <Button
          className={styles.remove}
          icon={<Remove />}
          onClick={onRemove}
        />
      )}
    </span>
  );
};
