import { TagBadge, useDappletTags } from '@/entities/Tag';
import { List } from '@/shared/components';

type DappletTagsProps = {
  tagIds: string[];
};

export const DappletTags = ({ tagIds }: DappletTagsProps) => {
  const tags = useDappletTags(tagIds);

  return (
    <List direction='row' gap='extrasmall'>
      {tags.map((tag) => (
        <List.Item key={tag.id}>
          <TagBadge label={tag.name} />
        </List.Item>
      ))}
    </List>
  );
};
