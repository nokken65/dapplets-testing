import DndIcon from '@icons/dnd.svg';
import { useState } from 'react';

import {
  Dapplet,
  DappletDetails,
  DappletImage,
  DappletInfo,
} from '@/entities/Dapplet';
import { Button, List } from '@/shared/components';
import { useDappletFields } from '@/shared/hooks';

import { DappletTags } from './DappletTags';
import styles from './index.module.scss';

type DappletItemProps = {
  dapplet: Dapplet;
};

export const DappletItem = ({ dapplet }: DappletItemProps) => {
  const [isExpanded, setIsExpanded] = useState<boolean>(false);
  const details = useDappletFields(dapplet);

  return (
    <List.Item
      className={styles.item}
      onClick={() => setIsExpanded(!isExpanded)}
    >
      <div className={styles.dapplet}>
        <DndIcon />
        <DappletImage alt={dapplet.title} name={dapplet.icon} />
        <DappletInfo
          address={dapplet.address}
          author={dapplet.author}
          description={dapplet.description}
          title={dapplet.title}
        />
        <DappletTags tagIds={dapplet.tags} />
        <Button label='INSTALL' />
      </div>
      {isExpanded && <DappletDetails fields={details} />}
    </List.Item>
  );
};
