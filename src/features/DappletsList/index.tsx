import { Dapplet, dappletModel } from '@/entities/Dapplet';
import { List, Loader, Reload } from '@/shared/components';

import { DappletItem } from './DappletItem';

type DappletsListWrapperProps = {
  dapplets: Dapplet[];
};

export const DappletsListView = ({ dapplets }: DappletsListWrapperProps) => {
  return (
    <List>
      {dapplets.map((dapplet) => (
        <DappletItem dapplet={dapplet} key={dapplet.id} />
      ))}
    </List>
  );
};

export const DappletsList = () => {
  const { data, isLoading, isError, refetch } =
    dappletModel.api.useGetDappletsQuery({});
  const dapplets = data?.data ?? [];

  if (isError) {
    return <Reload onReload={refetch} />;
  }
  if (isLoading) {
    return <Loader.Shine />;
  }

  return <DappletsListView dapplets={dapplets} />;
};
