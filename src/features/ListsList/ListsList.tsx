import { Lists, listsModel } from '@/entities/List';
import { Loader, Reload } from '@/shared/components';

export const ListsList = () => {
  const { data, isLoading, isError, refetch } =
    listsModel.api.useGetListsQuery();
  const lists = data?.data ?? [];

  if (isError) {
    return <Reload onReload={refetch} />;
  }
  if (isLoading) {
    return <Loader.Shine />;
  }

  return <Lists.List lists={lists} />;
};
