import { Tag, TagBadge, tagsModel } from '@/entities/Tag';
import { List, Loader, Reload } from '@/shared/components';

type TagsListViewProps = {
  tags: Tag[];
};

export const TagsListView = ({ tags }: TagsListViewProps) => {
  return (
    <List direction='row' gap='small'>
      {tags.map((tag) => (
        <List.Item key={tag.id}>
          <TagBadge label={tag.name} />
        </List.Item>
      ))}
    </List>
  );
};

export const TagsList = () => {
  const { data, isLoading, isError, refetch } = tagsModel.api.useGetTagsQuery();
  const tags = data?.data ?? [];

  if (isError) {
    return <Reload onReload={refetch} />;
  }
  if (isLoading) {
    return <Loader.Shine />;
  }

  return <TagsListView tags={tags} />;
};
