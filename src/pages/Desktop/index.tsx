import { DappletsFeed } from '@/widgets/DappletsFeed';
import { Header } from '@/widgets/Header';
import { LeftMenu } from '@/widgets/LeftMenu';
import { SettingsMenu } from '@/widgets/SettingsMenu';

import styles from './index.module.scss';

const DesktopPage = () => {
  return (
    <div className={styles.desktop}>
      <LeftMenu />
      <Header />
      <DappletsFeed />
      <SettingsMenu />
    </div>
  );
};

export default DesktopPage;
