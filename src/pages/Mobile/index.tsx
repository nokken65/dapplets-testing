import { Logo } from '@/shared/components';

const MobilePage = () => {
  return (
    <div>
      <h1>MobilePage</h1>
      <Logo />
    </div>
  );
};

export default MobilePage;
