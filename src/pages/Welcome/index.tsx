import { Link } from 'react-router-dom';

import { Mode } from '@/shared/constants';

import styles from './index.module.scss';

type WelcomePageProps = {
  setMode: (mode: Mode) => void;
};

const WelcomePage = ({ setMode }: WelcomePageProps) => {
  return (
    <div className={styles.welcome}>
      <div className={styles.welcome_card}>
        <div className={styles.welcome_header}>
          <h1 className={styles.welcome_heading}>Добро пожаловать</h1>
          <span className={styles.welcome_logo}>Dapplets</span>
        </div>
        <div className={styles.welcome_main}>
          <p>в тестовое задание на должность Frontend Developer</p>
          <p>
            Мы строим платформу Аугментированного веба, состоящую из браузерного
            плагина и децентрализованных приложений (дапплетов), основанных на
            крипто-технологиях.
          </p>
          <p>
            Наша платформа создается по принципу open-source и доступна для
            разработчиков со всего мира.
          </p>
        </div>
        <div className={styles.welcome_footer}>
          <Link
            className={styles.welcome_button}
            to='/'
            onClick={() => setMode(Mode.Desktop)}
          >
            Desktop
          </Link>
          <Link
            className={styles.welcome_button}
            to='/m'
            onClick={() => setMode(Mode.Mobile)}
          >
            Mobile
          </Link>
        </div>
      </div>
    </div>
  );
};

export default WelcomePage;
