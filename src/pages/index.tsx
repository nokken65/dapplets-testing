import { lazy, Suspense } from 'react';
import { Navigate, Route, Routes } from 'react-router-dom';

import { tagsModel } from '@/entities/Tag';
import { Loader } from '@/shared/components';
import { Mode } from '@/shared/constants';
import { useLocalStorage } from '@/shared/hooks';

const MobilePage = lazy(() => import('./Mobile'));
const DesktopPage = lazy(() => import('./Desktop'));
const WelcomePage = lazy(() => import('./Welcome'));

const Routing = () => {
  const [mode, setMode] = useLocalStorage<string>('mode', '');
  const redirectUrl = mode ? (mode === Mode.Desktop ? '/' : '/m') : '/welcome';

  return (
    <Suspense fallback={<Loader.Circle />}>
      <Routes>
        {mode === Mode.Desktop && <Route element={<DesktopPage />} path='/' />}
        {mode === Mode.Mobile && <Route element={<MobilePage />} path='/m' />}

        <Route element={<WelcomePage setMode={setMode} />} path='/welcome' />
        <Route element={<Navigate replace to={redirectUrl} />} path='/*' />
      </Routes>
    </Suspense>
  );
};

export { Routing };
