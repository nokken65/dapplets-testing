import { BaseQueryFn } from '@reduxjs/toolkit/query';
import axios, { AxiosError, AxiosRequestConfig, ResponseType } from 'axios';

import { InvalidResponse, ValidResponse } from './models';

type AxiosBaseQueryParams = {
  baseUrl: string;
  paramsSerializer?: (params: any) => string;
};

type AxiosBaseQueryArgs = {
  url: string;
  method: AxiosRequestConfig['method'];
  data?: AxiosRequestConfig['data'];
  params?: AxiosRequestConfig['params'];
  timeout?: number;
  responseType?: ResponseType;
};

export const axiosBaseQuery =
  ({
    baseUrl = '',
    paramsSerializer,
  }: AxiosBaseQueryParams): BaseQueryFn<
    AxiosBaseQueryArgs,
    ValidResponse<any>,
    InvalidResponse
  > =>
  async ({ url, method, data, params, timeout, responseType }) => {
    try {
      const res = await axios({
        url: baseUrl + url,
        method,
        data,
        params,
        paramsSerializer,
        timeout,
        responseType,
      });

      return {
        data: {
          status: res.status,
          success: true,
          data: res.data,
          ...res.data,
        },
      };
    } catch (axiosError) {
      const err = axiosError as AxiosError;

      return {
        error: {
          status: err.response?.status || 499,
          success: false,
          url: baseUrl + url,
          message: err.message,
          ...err.response?.data,
        },
      };
    }
  };
