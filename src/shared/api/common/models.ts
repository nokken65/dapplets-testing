export type InvalidResponse = {
  status: number;
  success?: boolean;
  url: string;
  message: string;
};

export type ValidResponse<T> = {
  status: number;
  success?: boolean;
  data: T;
  total?: number;
};
