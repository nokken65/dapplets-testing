import { axiosBaseQuery } from '../common';

export const dappletsBaseQuery = axiosBaseQuery({
  baseUrl: 'https://dapplets-hiring-api.herokuapp.com/api/v1/',
});
