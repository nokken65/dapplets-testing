import { axiosBaseQuery } from '../common';

export const mocksBaseQuery = axiosBaseQuery({
  baseUrl: '/data/',
});
