import clsx from 'clsx';
import { HTMLAttributes, memo } from 'react';

import styles from './Button.module.scss';

type ButtonProps = HTMLAttributes<HTMLButtonElement> & {
  icon?: JSX.Element;
  label?: string;
};

export const Button = memo(
  ({ icon, label, className, ...props }: ButtonProps) => {
    return (
      <button
        className={clsx(styles.button, className)}
        type='button'
        {...props}
      >
        {icon}
        {label && <p>{label}</p>}
      </button>
    );
  },
);
