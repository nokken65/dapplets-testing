import clsx from 'clsx';

import styles from './Heading.module.scss';

type HeadingProps = {
  title: string;
  uppercase?: boolean;
  size?: 'medium' | 'large';
};

export const Heading = ({
  title,
  uppercase = false,
  size = 'medium',
}: HeadingProps) => {
  return (
    <h2
      className={clsx(
        styles.heading,
        uppercase && styles.uppercase,
        styles[size],
      )}
    >
      {title}
    </h2>
  );
};
