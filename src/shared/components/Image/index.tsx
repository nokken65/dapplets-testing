import { DetailedHTMLProps, ImgHTMLAttributes } from 'react';

type ImageProps = DetailedHTMLProps<
  ImgHTMLAttributes<HTMLImageElement>,
  HTMLImageElement
>;

export const Image = ({ alt = 'image', ...props }: ImageProps) => {
  return <img alt={alt} {...props} />;
};
