import clsx from 'clsx';
import { PropsWithChildren } from 'react';

import { LayoutProps } from '../types';
import styles from './Aside.module.scss';

type AsideProps = PropsWithChildren<LayoutProps>;

export const Aside = ({
  children,
  className,
  gridArea = 'aside',
}: AsideProps) => {
  return (
    <aside className={clsx(styles.aside, className)} style={{ gridArea }}>
      {children}
    </aside>
  );
};
