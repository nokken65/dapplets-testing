import { PropsWithChildren } from 'react';

import { LayoutProps } from '../types';
import styles from './Header.module.scss';

type HeaderProps = PropsWithChildren<LayoutProps>;

export const Header = ({ children, gridArea = 'header' }: HeaderProps) => {
  return (
    <header className={styles.header} style={{ gridArea }}>
      {children}
    </header>
  );
};
