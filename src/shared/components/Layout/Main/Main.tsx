import clsx from 'clsx';
import { PropsWithChildren } from 'react';

import { LayoutProps } from '../types';
import styles from './Main.module.scss';

type MainProps = PropsWithChildren<LayoutProps>;

export const Main = ({ children, className, gridArea = 'main' }: MainProps) => {
  return (
    <main className={clsx(styles.main, className)} style={{ gridArea }}>
      {children}
    </main>
  );
};
