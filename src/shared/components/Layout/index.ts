import { Aside } from './Aside';
import { Header } from './Header';
import { Main } from './Main';

export const Layout = {
  Header,
  Main,
  Aside,
};
