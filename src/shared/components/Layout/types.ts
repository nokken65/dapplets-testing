export type LayoutProps = {
  gridArea?: string;
  className?: string;
};
