import clsx from 'clsx';
import { HTMLAttributes } from 'react';

import styles from './index.module.scss';

type ItemProps = HTMLAttributes<HTMLLIElement>;

export const Item = ({ children, className, ...props }: ItemProps) => {
  return (
    <li className={clsx(styles.item, className)} {...props}>
      {children}
    </li>
  );
};
