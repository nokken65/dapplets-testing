import clsx from 'clsx';
import { HTMLAttributes } from 'react';

import { Item } from './Item';
import styles from './List.module.scss';

type ListProps = HTMLAttributes<HTMLUListElement> & {
  direction?: 'row' | 'column';
  gap?: 'extrasmall' | 'small' | 'medium';
};

const List = ({
  direction = 'column',
  gap = 'medium',
  children,
  className,
  ...props
}: ListProps) => {
  return (
    <ul
      className={clsx(
        styles.list,
        styles[direction],
        styles[`gap_${gap}`],
        className,
      )}
      {...props}
    >
      {children}
    </ul>
  );
};

List.Item = Item;

export { List };
