import clsx from 'clsx';

import styles from './index.module.scss';

type ShineProps = {
  className?: string;
};

export const Shine = ({ className }: ShineProps) => {
  return <span className={clsx(styles.box, className)} />;
};
