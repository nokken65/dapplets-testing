import { Circle } from './Circle';
import { Shine } from './Shine';

export const Loader = {
  Shine,
  Circle,
};
