// eslint-disable-next-line import/no-unresolved
import logo from '@assets/images/logo.png?as=webp';
import clsx from 'clsx';
import { memo } from 'react';

import styles from './Logo.module.scss';

type LogoProps = {
  expanded?: boolean;
};

export const Logo = memo(({ expanded = true }: LogoProps) => {
  return (
    <div className={clsx(styles.logo, !expanded && styles.removeMargins)}>
      <img alt='logo' height={50} loading='lazy' src={logo} width={50} />
      {expanded && (
        <h1 className={styles.title}>
          Dapplets Project <b>.</b>
        </h1>
      )}
    </div>
  );
});
