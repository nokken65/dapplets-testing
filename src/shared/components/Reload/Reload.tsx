import ReloadIcon from '@icons/reload.svg';

import styles from './Reload.module.scss';

type ReloadProps = {
  onReload: () => void;
};

export const Reload = ({ onReload }: ReloadProps) => {
  return (
    <span
      className={styles.reload}
      role='button'
      tabIndex={0}
      onClick={onReload}
      onKeyDown={onReload}
    >
      <p>error :(</p>
      <ReloadIcon height={50} width={50} />
    </span>
  );
};
