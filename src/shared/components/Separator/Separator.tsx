import styles from './Separator.module.scss';

export const Separator = () => {
  return <span className={styles.separator} />;
};
