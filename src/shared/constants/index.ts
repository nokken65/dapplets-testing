/* eslint-disable no-unused-vars */
export enum Status {
  Loading,
  Succeeded,
  Failed,
}

export enum Mode {
  Desktop = 'desktop',
  Mobile = 'mobile',
}
