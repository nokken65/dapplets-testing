import { useMemo } from 'react';

import { Dapplet, DappletField } from '@/entities/Dapplet';

const titles: string[] = [
  'Aliquam sit',
  'Semper neque',
  'Leo ipsum.',
  'Elit sagittis et.',
  'Aliquam.',
  'In euismod.',
  'Justo amet.',
  'Urna.',
  'Nam diam.',
];

const useDappletFields = (dapplet: Dapplet): DappletField[] => {
  const fields = useMemo<DappletField[]>(() => {
    try {
      return titles.map((title, i) => {
        const key = `text_${i + 1}`;

        return { title, content: dapplet[key] as string };
      });
    } catch (error) {
      console.error(error);

      return [];
    }
  }, [dapplet]);

  return fields;
};

export { useDappletFields };
