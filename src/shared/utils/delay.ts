/* eslint-disable no-return-await */
/* eslint-disable no-promise-executor-return */
/* eslint-disable @typescript-eslint/no-explicit-any */
export const delay = async (timeout: number): Promise<void> =>
  await new Promise((resolve) =>
    setTimeout((res: any) => resolve(res), timeout),
  );
