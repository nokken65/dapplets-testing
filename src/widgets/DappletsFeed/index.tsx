import { DappletsList } from '@/features/DappletsList';
import { Layout, Separator } from '@/shared/components';

import styles from './index.module.scss';

export const DappletsFeed = () => {
  return (
    <Layout.Main>
      <div className={styles.feed}>
        <h2>header</h2>
        <Separator />
        <DappletsList />
      </div>
    </Layout.Main>
  );
};
