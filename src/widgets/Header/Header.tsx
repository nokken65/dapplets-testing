// import { ErrorsStatus } from '@/features/ErrorsStatus';
import { SwitchButton } from '@/entities/Menu';
import { SwitchButton as SwitchSettings } from '@/entities/Settings';
import { Layout } from '@/shared/components';

export const Header = () => {
  return (
    <Layout.Header>
      <SwitchButton />
      {/* <ErrorsStatus /> */}
      <SwitchSettings />
    </Layout.Header>
  );
};
