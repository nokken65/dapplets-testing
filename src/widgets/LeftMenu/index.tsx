import clsx from 'clsx';

import { menuModel } from '@/entities/Menu';
import { ListsList } from '@/features/ListsList';
import { TagsList } from '@/features/TagsList';
import { Heading, Layout, Logo, Separator } from '@/shared/components';
import { Nav } from '@/widgets/Nav';

import styles from './index.module.scss';

export const LeftMenu = () => {
  const { isExpanded } = menuModel.useMenu();

  return (
    <Layout.Aside
      className={clsx(styles.menu, isExpanded && styles.isExpanded)}
      gridArea='menu'
    >
      <Logo expanded={isExpanded} />
      <Nav />
      {isExpanded && (
        <>
          <Separator />
          <Heading title='My lists' />
          <ListsList />
          <Separator />
          <Heading title='My tags' />
          <TagsList />
        </>
      )}
    </Layout.Aside>
  );
};
