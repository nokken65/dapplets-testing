import { nanoid } from '@reduxjs/toolkit';

import { menuModel } from '@/entities/Menu';
import { List } from '@/shared/components';

import { menuItemsData } from './menuItemsData';
import { NavItem } from './NavItem';

const Nav = () => {
  const { isExpanded } = menuModel.useMenu();

  return (
    <List>
      {menuItemsData.map((item, i) => (
        <li key={nanoid()}>
          <NavItem
            active={i === 1}
            isExpanded={isExpanded}
            item={item}
            onActive={() => null}
          />
        </li>
      ))}
    </List>
  );
};

export { Nav };
