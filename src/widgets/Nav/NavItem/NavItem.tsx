import clsx from 'clsx';
import { memo } from 'react';

import { Button } from '@/shared/components';

import { MenuItemData } from '../menuItemsData';
import styles from './NavItem.module.scss';

type NavItemProps = {
  item: MenuItemData;
  isExpanded: boolean;
  active?: boolean;
  onActive: () => void;
};

export const NavItem = memo(
  ({ item, isExpanded, active, onActive }: NavItemProps) => {
    return (
      <Button
        className={clsx(
          styles.item,
          active && styles.active,
          !isExpanded && styles.isNotExpanded,
        )}
        icon={item.icon}
        label={isExpanded ? item.title : undefined}
        onClick={onActive}
      />
    );
  },
);
