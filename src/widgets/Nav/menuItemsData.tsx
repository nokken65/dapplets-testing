import Dapplets from '@icons/dapplets.svg';
import Grid from '@icons/grid.svg';
import Heart from '@icons/heart.svg';
import TrendingUp from '@icons/trending-up.svg';
import Users from '@icons/users.svg';

export type MenuItemData = {
  title: string;
  icon: JSX.Element;
};

export const menuItemsData: MenuItemData[] = [
  {
    title: 'All Dapplets',
    icon: <Dapplets />,
  },
  {
    title: 'Editor’s Choice',
    icon: <Heart />,
  },
  {
    title: 'Essential Dapplets',
    icon: <Grid />,
  },
  {
    title: 'Social Networks',
    icon: <Users />,
  },
  {
    title: 'Financial Dapplets',
    icon: <TrendingUp />,
  },
];
