import { CloseButton, settingsModel } from '@/entities/Settings';
import { Heading, Layout } from '@/shared/components';

import styles from './index.module.scss';

export const SettingsMenu = () => {
  const { isExpanded } = settingsModel.useSettings();

  return isExpanded ? (
    <Layout.Aside className={styles.settings} gridArea='settings'>
      <CloseButton />
      <Heading uppercase size='large' title='Dapplets settings' />
      <Heading uppercase size='large' title='My tags' />
      <Heading uppercase size='large' title='Community tags' />
      <Heading uppercase size='large' title='Working on' />
    </Layout.Aside>
  ) : null;
};
