import FaviconsWebpackPlugin from 'favicons-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import { cpus } from 'os';
import { resolve } from 'path';
import { Configuration, DefinePlugin, ProvidePlugin } from 'webpack';

import { CACHE, FAVICON, PUBLIC, ROOT, SRC } from './constants';

export const commonConfig: Configuration = {
  context: ROOT,
  stats: 'normal',
  parallelism: cpus().length - 1,
  cache: {
    type: 'filesystem',
    cacheDirectory: CACHE,
    buildDependencies: {
      config: [__filename],
    },
  },
  entry: { app: resolve(SRC, './index.tsx') },
  resolve: {
    extensions: ['.js', '.jsx', '.ts', '.tsx'],
    alias: {
      '@': SRC,
      '@icons': resolve(PUBLIC, 'icons'),
      '@assets': resolve(PUBLIC, 'assets'),
    },
  },
  plugins: [
    new HtmlWebpackPlugin({
      template: resolve(PUBLIC, 'index.html'),
      inject: 'body',
    }),
    new FaviconsWebpackPlugin({
      logo: FAVICON,
      mode: 'light',
    }),
    new ProvidePlugin({
      React: 'react',
    }),
    new DefinePlugin({
      'process.env': JSON.stringify(process.env),
    }),
  ],
  module: {
    rules: [
      {
        test: /\.ts(x)?$/,
        loader: 'esbuild-loader',
        options: { loader: 'tsx', target: 'es2015' },
        include: SRC,
        exclude: /node_modules/,
      },
      {
        test: /\.(webp|png|jpe?g|gif)$/i,
        type: 'asset/resource',
      },
      {
        test: /\.woff($|\?)|\.woff2($|\?)|\.ttf($|\?)|\.eot($|\?)/i,
        type: 'asset/resource',
      },
      {
        test: /\.svg$/i,
        issuer: /\.[jt]sx?$/,
        use: [
          {
            loader: '@svgr/webpack',
            options: {
              prettier: true,
              memo: true,
              svgo: true,
              icon: true,
              typescript: true,
            },
          },
        ],
      },
    ],
  },
};
