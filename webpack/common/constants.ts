import { resolve } from 'path';

const ROOT = resolve(__dirname, '../../');
const SRC = resolve(ROOT, 'src');
const DIST = resolve(ROOT, 'build');
const PUBLIC = resolve(ROOT, 'public');
const FAVICON = resolve(ROOT, 'public', 'favicon.svg');
const CACHE = resolve(ROOT, '.cache');

export { CACHE, DIST, FAVICON, PUBLIC, ROOT, SRC };
