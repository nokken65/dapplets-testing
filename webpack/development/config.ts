import { resolve } from 'path';
import { Configuration, RuleSetRule } from 'webpack';
import { Configuration as ConfigurationDevServer } from 'webpack-dev-server';

import { DIST } from '../common/constants';
import { devServer } from './devServer';
import { plugins } from './plugins';
import { cssRule, sassModuleRule, sassRule } from './rules';

type DevConfig = Configuration & {
  devServer?: ConfigurationDevServer;
};

const rules: RuleSetRule[] = [cssRule, sassRule, sassModuleRule];

const devConfig: DevConfig = {
  mode: 'development',
  output: {
    path: resolve(DIST),
    publicPath: '/',
    filename: 'bundle.js',
    assetModuleFilename: 'assets/[name][ext]',
  },
  devtool: 'eval-cheap-module-source-map',
  devServer,
  plugins,
  module: { rules },
};

export { devConfig };
