import ReactRefreshWebpackPlugin from '@pmmmwh/react-refresh-webpack-plugin';
import { WebpackPluginInstance } from 'webpack';

const plugins: WebpackPluginInstance[] = [new ReactRefreshWebpackPlugin()];

export { plugins };
