import CssMinimizerPlugin from 'css-minimizer-webpack-plugin';
import ImageMinimizerPlugin from 'image-minimizer-webpack-plugin';
import { cpus } from 'os';
import { resolve } from 'path';
import TerserPlugin from 'terser-webpack-plugin';
import { Configuration, RuleSetRule } from 'webpack';

import { dependencies } from '../../package.json';
import { DIST, PUBLIC, SRC } from '../common/constants';
import { plugins } from './plugins';
import { cssRule, sassModuleRule, sassRule } from './rules';

const rules: RuleSetRule[] = [cssRule, sassRule, sassModuleRule];

const deps = Object.keys(dependencies);

const prodConfig: Configuration = {
  mode: 'production',
  entry: {
    vendor: deps,
    app: resolve(SRC, './index.tsx'),
  },
  output: {
    path: resolve(DIST),
    publicPath: '/',
    filename: '[name].[contenthash].js',
    clean: true,
    assetModuleFilename: 'assets/[hash][ext][query]',
  },
  devtool: false,
  plugins,
  module: { rules },
  optimization: {
    moduleIds: 'deterministic',
    runtimeChunk: 'single',
    splitChunks: {
      chunks: 'all',
      minSize: 20000,
      minRemainingSize: 0,
      minChunks: 1,
      maxAsyncRequests: 30,
      maxInitialRequests: 30,
      enforceSizeThreshold: 50000,
      cacheGroups: {
        reactVendor: {
          test: /[\\/]node_modules[\\/](react|react-dom)[\\/]/,
          priority: -10,
          reuseExistingChunk: true,
        },
        reduxVendor: {
          test: /[\\/]node_modules[\\/](@reduxjs|redux|react-redux)[\\/]/,
          priority: -20,
          reuseExistingChunk: true,
        },
        defaultVendors: {
          test: /[\\/]node_modules[\\/](!react)(!react-dom)(!@reduxjs)(!redux)(!react-redux)[\\/]/,
          priority: -30,
          reuseExistingChunk: true,
        },
        default: {
          minChunks: 2,
          priority: -20,
          reuseExistingChunk: true,
        },
      },
    },
    minimize: true,
    minimizer: [
      new TerserPlugin({
        parallel: true,
        terserOptions: {
          ecma: 2015,
          compress: true,
          mangle: true,
        },
      }),
      new CssMinimizerPlugin(),
      new ImageMinimizerPlugin({
        test: /.(jpe?g|png|gif|tif|webp|avif)$/i,
        concurrency: cpus().length - 1,
        include: resolve(PUBLIC, 'assets'),
        exclude: resolve(PUBLIC, 'icons'),
        generator: [
          {
            preset: 'webp',
            type: 'import',
            implementation: ImageMinimizerPlugin.imageminGenerate,
            filename: '[name][ext]',
            options: {
              plugins: ['imagemin-webp'],
            },
          },
        ],
        minimizer: {
          implementation: ImageMinimizerPlugin.imageminMinify,
          options: {
            plugins: [
              ['imagemin-pngquant', { quality: [0.3, 0.5] }],
              ['imagemin-webp', { quality: 75, preset: 'picture' }],
            ],
          },
        },
      }),
    ],
  },
};

export { prodConfig };
